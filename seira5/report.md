---
mainfont: Open Sans
documentclass: extarticle
fontsize: 12pt
geometry: margin=1in
papersize: a4
title: "5η Σειρά ασκήσεων στα Συστήματα Μικροϋπολογιστών"
author:
- "Ελευθέριος Καπελώνης 03116163"
- "Κωνσταντίνος Αγιάννης 03116352"
---

# 2η Άσκηση

```asm
org 100h

start:
	; read
	mov ah,1
	int 21h
	mov bh,al
	int 21h
	mov bl,al
	int 21h
	mov ch,al
	int 21h
	mov cl,al
	
	; write Z=
	mov ah,9
	mov dx,msg1
	int 21h
	
	mov ah, 2
	mov dl,bh
	int 21h
	mov dl,bl
	int 21h

	; write W=
	mov ah,9
	mov dx,msg2
	int 21h

	mov ah,2
	mov dl, ch
	int 21h
	mov dl,cl
	int 21h
	
	; print newline
	mov dl,0xa
	int 21h
	mov dl,0xd
	int 21h
	
	; convert to binary
	sub cl,'0'
	sub ch,'0'
	sub bl,'0'
	sub bh,'0'
	
	; addition

	mov al, ch
	mov ah,0

	mov dx,10

	mul dl ; ax *= 10
	mov ch,0
	add cx,ax ; cx now contains the number

	mov al,bh
	mov ah,0
	mul dl ; ax *= 10
	mov bh,0
	add ax,bx
	mov si,ax ; si now has the number
	
	mov ah,9
	mov dx,msg3
	int 21h ; print Z+W
	
	mov bx,si
	add bx,cx
	
	shr bx,4
	and bx,15
	mov dl,[bx + digits]
	mov ah, 2
	int 21h

	mov bx,si
	add bx,cx
	and bx,15
	mov dl, [digits + bx]
	int 21h

	; ###################################################
	mov ah,9
	mov dx,msg4
	int 21h ; print Z-W
	mov ah,2
	mov bx,si
	sub bx,cx
	
	mov si,bx
	cmp bx,0
	jge not_negative
	mov dl,'-'
	int 21h
	neg si
	neg bx
not_negative: 
	shr bx,4
	and bx, 15
	mov dl,[bx + digits]
	int 21h

	mov bx,si
	and bx,15
	mov dl, [digits + bx]
	int 21h
	jmp start

msg1 db 0xa,0xd,"Z=$"
msg2 db " W=$"

msg3 db "Z+W=$"
msg4 db " Z-W=$"
digits db "0123456789ABCDEF"
```

# 3η Άσκηση

```asm
MAIN PROC NEAR

START:  
        CALL HEX_KEYB       ; first hex
        CMP AL, 'T'
        JE STOP
        MOV BL, AL          ; first hex in BL
        CALL HEX_KEYB       ; second hex in AL
        CMP AL, 'T'
        JE STOP
        MOV CL, 4
        SHL BL, CL
        OR BL, AL
        
        MOV DL, '='
        MOV AH, 2
        INT 21H             ; print '='
        
        CALL PRINT_DEC
         
        MOV DL, '='         ; print '='
        MOV AH, 2  
        INT 21H
        
        CALL PRINT_OCT
         
        MOV DL, '='         ; print '='
        MOV AH, 2  
        INT 21H
                
        CALL PRINT_BIN
        
        MOV DL, ' '         ; print ' '
        MOV AH, 2  
        INT 21H
              
        JMP START
STOP:   HLT    
MAIN ENDP

PRINT_DEC PROC NEAR
        PUSH AX
        PUSH BX
        PUSH DX
        
        MOV AH, 0
        MOV AL, BL   
        
        MOV DL, 100       
        DIV DL      ; AL -> ekatontades
        
        MOV DL, AL
        ADD DL, 30H ; DL -> ASCII ekatontades
        MOV BL, AH  ; BL -> ypoloipo
        
        MOV AH, 2
        INT 21H     ; print
        
        MOV AL, BL  ; ypoloipo
        MOV AH, 0 
        MOV DL, 10
        DIV DL      ; AL -> dekades kai AH -> monades
        
        MOV DL, AL
        ADD DL, 30H ; DL -> ASCII dekades
        MOV BL, AH  ; BL -> monades
        
        MOV AH, 2
        INT 21H     ; print
        
        MOV DL, BL
        ADD DL, 30H ; DL -> ASCII monades
        MOV AH, 2
        INT 21H     ; print
          
        POP DX
        POP BX     
        POP AX
        RET
PRINT_DEC ENDP


PROC PRINT_BIN NEAR
        PUSH AX
        PUSH BX
        PUSH CX
        PUSH DX
        MOV CX, 8
ADDR:   MOV DL, 0   
        SHL BL, 1

        ADC DL, 30H
        MOV AH, 2
        INT 21H
        LOOP ADDR
        
        POP DX
        POP CX
        POP BX
        POP AX
        RET
PRINT_BIN ENDP 

  
PROC PRINT_OCT NEAR
        PUSH AX
        PUSH BX
        PUSH DX
        
        MOV AH, 0
        MOV AL, BL
        
        MOV DL, 64  ; 8^2
        DIV DL
        
        MOV DL, AL
        ADD DL, 30H
        MOV BL, AH
        
        MOV AH, 2
        INT 21H
        
        MOV AL, BL
        MOV AH, 0
        MOV DL, 8
        DIV DL
        
        MOV DL, AL
        ADD DL, 30H
        MOV BL, AH
        
        MOV AH, 2
        INT 21H
        
        MOV DL, BL
        ADD DL, 30H
        MOV AH, 2
        INT 21H
        
        POP DX
        POP BX
        POP AX
        RET
PRINT_OCT ENDP

HEX_KEYB PROC NEAR
    
        PUSH DX
IGNORE: 
        MOV AH, 8       ; read to AL
        INT 21H
        
        CMP AL, 'T'
        JE ADDR2
        
        CMP AL, 30H
        JL IGNORE
        CMP AL, 39H
        JG ADDR1
        PUSH AX
        
        MOV DL, AL      ; print 0-9
        MOV AH, 2
        INT 21H
        
        POP AX
        SUB AL, 30H
        JMP ADDR2
 ADDR1: CMP AL, 'A'
        JL IGNORE
        CMP AL, 'F'
        JG IGNORE
        PUSH AX
        
        MOV DL, AL      ; print A-F
        MOV AH, 2
        INT 21H
       
        POP AX
        SUB AL, 37H
ADDR2:  POP DX
        RET
HEX_KEYB ENDP
```

# 4η Άσκηση

```asm
DATA_SEG SEGMENT
    ARRAY DB 20 DUP(?)         ; store numbers
DATA_SEG ENDS

CODE_SEG SEGMENT
    ASSUME CS:CODE_SEG, DS:DATA_SEG


READ MACRO
    MOV AH, 8
    INT 21H
ENDM

PRINT MACRO                 ; character in DL
    MOV AH, 2
    INT 21H
ENDM


MAIN PROC FAR       
        MOV AX, DATA_SEG
        MOV DS, AX
        MOV ES, AX 
START:  
        MOV AH, 0
        MOV BX, 0           ; how many numbers
        
NEXT:   READ
        
        CMP AL, 13          ; check enter character
        JE PRINT_UP         ; print uppercase letters
        
        CMP AL, '='
        JE STOP 
         
        CMP AL, '0'
        JL NEXT
        CMP AL, '9'
        JLE DIGIT
        
        CMP AL, 'a'
        JL NEXT
        CMP AL, 'z'
        JG NEXT
        
LETTER: 
        TEST BL, 1          ; test if even
        JNE SAVE1
        MOV DL, AL
        PRINT               ; print if BL is odd

SAVE1:  SUB AL, 20h         ; convert to uppercase
        JMP SAVE2    
        
DIGIT:
        TEST BL, 1          ; test if even
        JNE SAVE2
        MOV DL, AL
        PRINT               ; print if BL is odd  

SAVE2:  
        MOV [BX], AL
        
        INC BL
        CMP BL, 20          ; repeat 20 times 
        JL NEXT
                
        
PRINT_UP:
        MOV DL, 13          ; print new line
        PRINT
        MOV DL, 10
        PRINT
        
        MOV DI, 0
        
PRINT_NEXT:
        MOV DX, [DI]
        CMP BX, DI
        JE DONE
        PRINT
        INC DI
        JMP PRINT_NEXT

DONE:   MOV DL, 13          ; print new line
        PRINT
        MOV DL, 10
        PRINT
        
        JMP START           ; infinite loop
        
STOP:   HLT 
   
MAIN ENDP

CODE_SEG ENDS
```
