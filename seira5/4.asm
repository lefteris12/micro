DATA_SEG SEGMENT
    ARRAY DB 20 DUP(?)         ; store numbers
DATA_SEG ENDS

CODE_SEG SEGMENT
    ASSUME CS:CODE_SEG, DS:DATA_SEG


READ MACRO
    MOV AH, 8
    INT 21H
ENDM

PRINT MACRO                 ; character in DL
    MOV AH, 2
    INT 21H
ENDM


MAIN PROC FAR       
        MOV AX, DATA_SEG
        MOV DS, AX
        MOV ES, AX 
START:  
        MOV AH, 0
        MOV BX, 0           ; how many numbers
        
NEXT:   READ
        
        CMP AL, 13          ; check enter character
        JE PRINT_UP         ; print uppercase letters
        
        CMP AL, '='
        JE STOP 
         
        CMP AL, '0'
        JL NEXT
        CMP AL, '9'
        JLE DIGIT
        
        CMP AL, 'a'
        JL NEXT
        CMP AL, 'z'
        JG NEXT
        
LETTER: 
        TEST BL, 1          ; test if even
        JNE SAVE1
        MOV DL, AL
        PRINT               ; print if BL is odd

SAVE1:  SUB AL, 20h         ; convert to uppercase
        JMP SAVE2    
        
DIGIT:
        TEST BL, 1          ; test if even
        JNE SAVE2
        MOV DL, AL
        PRINT               ; print if BL is odd  

SAVE2:  
        MOV [BX], AL
        
        INC BL
        CMP BL, 20          ; repeat 20 times 
        JL NEXT
                
        
PRINT_UP:
        MOV DL, 13          ; print new line
        PRINT
        MOV DL, 10
        PRINT
        
        MOV DI, 0
        
PRINT_NEXT:
        MOV DX, [DI]
        CMP BX, DI
        JE DONE
        PRINT
        INC DI
        JMP PRINT_NEXT

DONE:   MOV DL, 13          ; print new line
        PRINT
        MOV DL, 10
        PRINT
        
        JMP START           ; infinite loop
        
STOP:   HLT 
   
MAIN ENDP

CODE_SEG ENDS
