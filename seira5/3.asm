MAIN PROC NEAR

START:  
        CALL HEX_KEYB       ; first hex
        CMP AL, 'T'
        JE STOP
        MOV BL, AL          ; first hex in BL
        CALL HEX_KEYB       ; second hex in AL
        CMP AL, 'T'
        JE STOP
        MOV CL, 4
        SHL BL, CL
        OR BL, AL
        
        MOV DL, '='
        MOV AH, 2
        INT 21H             ; print '='
        
        CALL PRINT_DEC
         
        MOV DL, '='         ; print '='
        MOV AH, 2  
        INT 21H
        
        CALL PRINT_OCT
         
        MOV DL, '='         ; print '='
        MOV AH, 2  
        INT 21H
                
        CALL PRINT_BIN
        
        MOV DL, ' '         ; print ' '
        MOV AH, 2  
        INT 21H
              
        JMP START
STOP:   HLT    
MAIN ENDP

PRINT_DEC PROC NEAR
        PUSH AX
        PUSH BX
        PUSH DX
        
        MOV AH, 0
        MOV AL, BL   
        
        MOV DL, 100       
        DIV DL      ; AL -> ekatontades
        
        MOV DL, AL
        ADD DL, 30H ; DL -> ASCII ekatontades
        MOV BL, AH  ; BL -> ypoloipo
        
        MOV AH, 2
        INT 21H     ; print
        
        MOV AL, BL  ; ypoloipo
        MOV AH, 0 
        MOV DL, 10
        DIV DL      ; AL -> dekades kai AH -> monades
        
        MOV DL, AL
        ADD DL, 30H ; DL -> ASCII dekades
        MOV BL, AH  ; BL -> monades
        
        MOV AH, 2
        INT 21H     ; print
        
        MOV DL, BL
        ADD DL, 30H ; DL -> ASCII monades
        MOV AH, 2
        INT 21H     ; print
          
        POP DX
        POP BX     
        POP AX
        RET
PRINT_DEC ENDP


PROC PRINT_BIN NEAR
        PUSH AX
        PUSH BX
        PUSH CX
        PUSH DX
        MOV CX, 8
ADDR:   MOV DL, 0   
        SHL BL, 1

        ADC DL, 30H
        MOV AH, 2
        INT 21H
        LOOP ADDR
        
        POP DX
        POP CX
        POP BX
        POP AX
        RET
PRINT_BIN ENDP 

  
PROC PRINT_OCT NEAR
        PUSH AX
        PUSH BX
        PUSH DX
        
        MOV AH, 0
        MOV AL, BL
        
        MOV DL, 64  ; 8^2
        DIV DL
        
        MOV DL, AL
        ADD DL, 30H
        MOV BL, AH
        
        MOV AH, 2
        INT 21H
        
        MOV AL, BL
        MOV AH, 0
        MOV DL, 8
        DIV DL
        
        MOV DL, AL
        ADD DL, 30H
        MOV BL, AH
        
        MOV AH, 2
        INT 21H
        
        MOV DL, BL
        ADD DL, 30H
        MOV AH, 2
        INT 21H
        
        POP DX
        POP BX
        POP AX
        RET
PRINT_OCT ENDP

HEX_KEYB PROC NEAR
    
        PUSH DX
IGNORE: 
        MOV AH, 8       ; read to AL
        INT 21H
        
        CMP AL, 'T'
        JE ADDR2
        
        CMP AL, 30H
        JL IGNORE
        CMP AL, 39H
        JG ADDR1
        PUSH AX
        
        MOV DL, AL      ; print 0-9
        MOV AH, 2
        INT 21H
        
        POP AX
        SUB AL, 30H
        JMP ADDR2
 ADDR1: CMP AL, 'A'
        JL IGNORE
        CMP AL, 'F'
        JG IGNORE
        PUSH AX
        
        MOV DL, AL      ; print A-F
        MOV AH, 2
        INT 21H
       
        POP AX
        SUB AL, 37H
ADDR2:  POP DX
        RET
HEX_KEYB ENDP
