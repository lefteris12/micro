org 100h

start:
	; read
	mov ah,1
	int 21h
	mov bh,al
	int 21h
	mov bl,al
	int 21h
	mov ch,al
	int 21h
	mov cl,al
	
	; write Z=
	mov ah,9
	mov dx,msg1
	int 21h
	
	mov ah, 2
	mov dl,bh
	int 21h
	mov dl,bl
	int 21h

	; write W=
	mov ah,9
	mov dx,msg2
	int 21h

	mov ah,2
	mov dl, ch
	int 21h
	mov dl,cl
	int 21h
	
	; print newline
	mov dl,0xa
	int 21h
	mov dl,0xd
	int 21h
	
	; convert to binary
	sub cl,'0'
	sub ch,'0'
	sub bl,'0'
	sub bh,'0'
	
	; addition

	mov al, ch
	mov ah,0

	mov dx,10

	mul dl ; ax *= 10
	mov ch,0
	add cx,ax ; cx now contains the number

	mov al,bh
	mov ah,0
	mul dl ; ax *= 10
	mov bh,0
	add ax,bx
	mov si,ax ; si now has the number
	
	mov ah,9
	mov dx,msg3
	int 21h ; print Z+W
	
	mov bx,si
	add bx,cx
	
	shr bx,4
	and bx,15
	mov dl,[bx + digits]
	mov ah, 2
	int 21h

	mov bx,si
	add bx,cx
	and bx,15
	mov dl, [digits + bx]
	int 21h

	; ###################################################
	mov ah,9
	mov dx,msg4
	int 21h ; print Z-W
	mov ah,2
	mov bx,si
	sub bx,cx
	
	mov si,bx
	cmp bx,0
	jge not_negative
	mov dl,'-'
	int 21h
	neg si
	neg bx
not_negative: 
	shr bx,4
	and bx, 15
	mov dl,[bx + digits]
	int 21h

	mov bx,si
	and bx,15
	mov dl, [digits + bx]
	int 21h
	jmp start

msg1 db 0xa,0xd,"Z=$"
msg2 db " W=$"

msg3 db "Z+W=$"
msg4 db " Z-W=$"
digits db "0123456789ABCDEF"
