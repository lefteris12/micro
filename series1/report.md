---
mainfont: Open Sans 
documentclass: extarticle
fontsize: 12pt
geometry: margin=1in
papersize: a4
title: "1η Σειρά ασκήσεων στα Συστήματα Μικροϋπολογιστών"
author:
- "Ελευθέριος Καπελώνης 03116163"
- "Κωνσταντίνος Αγιάννης 03116352"
---

# 1η ΑΣΚΗΣΗ
Με τη βοήθεια του πίνακα των σημειώσεων βρίσκουμε το πρόγραμμα σε assembly:
```asm
        MVI C,08H  ; (C) = 08H
        LDA 2000H  ; fortwsi eisodou ston (A)
L2:     RAL        ; peristrofi tou (A) wste to MSB na apothikeutei sto (CY)
        JC L1      ; if (CY) = 1 jump to L1
        DCR C      ; (C) = (C) - 1
        JNZ L2     ; if (C) != 0 jump to L2
L1:     MOV A,C    ; (A) = (C) 
        CMA        ; simplirwma ws pros 1 tou (A) epeidi i eksodos einai arnitikis logikis
        STA 3000H  ; O (A) emfanizetai sta leds
        RST 1      ; epistrofi sto monitor program
        END
```

![Διάγραμμα ροής](flow_diagram.png)

Μπορούμε να τροποποιήσουμε τον κώδικα αντικαθιστώντας την εντολή ``` RST 1 ``` με μια ```JMP``` ώστε να επαναλαμβάνεται χωρίς τέλος:
```asm
START:  MVI C,08H  ; (C) = 08H
        LDA 2000H  ; fortwsi eisodou ston (A)
L2:     RAL        ; peristrofi tou (A) wste to MSB na apothikeutei sto (CY)
        JC L1      ; if (CY) = 1 jump to L1
        DCR C      ; (C) = (C) - 1
        JNZ L2     ; if (C) != 0 jump to L2
L1:     MOV A,C    ; (A) = (C) 
        CMA        ; simplirwma ws pros 1 tou (A) epeidi i eksodos einai arnitikis logikis
        STA 3000H  ; O (A) emfanizetai sta leds
        JMP START  ; antikatastasi tou RST 1 wste to programma na ekteleitai epanalambanomena
        END
```

Ο κώδικας προσομοιώθηκε στο πρόγραμμα TSIK. Το πρόγραμμα έχει την ακόλουθη
λειτουργία: O τελευταίος ενεργοποιημένος από τα δεξιά προς τα αριστερά διακόπτης εμφανίζεται με δυαδική μορφή στην έξοδο μέσω των leds. Για παράδειγμα αν είναι ενεργοποιημένος ο τρίτος από τα δεξιά προς τα αριστερά διακόπτης (και
κανένας μετά από αυτόν) τότε ανάβουν τα δύο πρώτα (από τα δεξιά προς τα
αριστερά) leds που αντιστοιχούν στον αριθμό 3 (11 στο δυαδικό σύστημα).

# 2η άσκηση
```asm
            IN 10H
            LXI B,01F4H ; apothikeusi tou 01F4 (500 sto dekadiko) sto zeugos BC
            MVI D,01H   ; o (D) periexei tin katastasi twn leds
START:      LDA 2000H   ; fortwsi eisodou ston (A)
            RAL         ; peristrofi tou (A) wste to MSB na apothikeutei sto (CY)
            MOV A,D     
            CMA         ; simplirwma ws pros 1 tou (A) epeidi i eksodos einai arnitikis logikis
            STA 3000H   ; O (A) emfanizetai sta leds
            CALL DELB   ; perimene 500ms
            JC RIGHT    ; if (CY) = 1 jump to RIGHT
            CMA         ; epanafora tou (A) 
            RLC         ; energopoiisi aristerou led     
            MOV D,A     ; apothikeusi tis kainourias katastasis twn leds
            JMP STOP    
RIGHT:      CMA         ; epanafora tou (A) 
            RRC         ; energopoiisi deksiou led 
            MOV D,A     ; apothikeusi tis kainourias katastasis twn leds
STOP:       LDA 2000H 
            RAR         ; peristrofi tou (A) wste to LSB na apothikeutei sto (CY)
            JC STOP     ; perimene mexri o LSB diakoptis na ginei OFF
            JMP START
            END
```

# 3η άσκηση
```asm
START:      LDA 2000H   ; fortwsi eisodou ston (A)
            CPI 64H     ; einai megaliteros i isos tou 100?
            JNC GREATER ; an nai pigaine sto GREATER
            MVI D,FFH   ; to FFH se simplirwma ws pros 2 einai to -1
LOOP1:      INR D
            SUI 0AH     ; substract 10
            JNC LOOP1   ; if positive continue
            ADI 0AH     ; if not correct the negative remainder
            MOV E,A     ; apothikeuse ton (A)
            MOV A,D     ; o (A) exei tis dekades
            RLC         ; peristrofi 4 fores wste o (A) na exei tis dekades sta 4 MSB
            RLC
            RLC
            RLC
            ORA E       ; o (A) exei tis monades sta 4 LSB 
            CMA         ; simplirwma ws pros 1 tou (A) epeidi i eksodos einai arnitikis logikis
            STA 3000H   ; emfanisi eksodou sta leds
            JMP START   ; elegkse tin eisodo ksana
GREATER:    LXI B,0500H ; orismos tou xronou kathisterisis tis routinas DELB
            MVI A,F0H   ; apothikeuse ston (A) to 11110000 wste na anapsoun ta 4 LSB leds
            STA 3000H   ; emfanisi eksodou sta leds
            CALL DELB   ; perimene
            MVI A,0FH   ; apothikeuse ston (A) to 00001111 wste na anapsoun ta 4 MSB leds
            STA 3000H   ; emfanisi eksodou sta leds
            CALL DELB   ; perimene
            JMP START   ; elegkse tin eisodo ksana
            END
```

<!---
plot2d([(20000 + 20*x)/x, (10000 + 40*x)/x,(100000 + 4*x)/x, (200000 + 2*x)/x ],[x,100,100000],[legend,"1st Technology","2nd Technology","3rd Technology","4th Technology"],[xlabel,"Number of products sold (log scale)"],[ylabel,"Cost of manufacture per product (log scale)"],logy,logx);
-->
# 4η άσκηση

![καμπύλες κόστους σε λογαριθμική κλίμακα](ask4.png)

**1η τεχνολογία** κόστος ανά μονάδα = (20,000 + 20*x)/x

**2η τεχνολογία** κόστος ανά μονάδα = (10,000 + 40*x)/x

**3η τεχνολογία** κόστος ανά μονάδα = (100,000 + 4*x)/x

**4η τεχνολογία** κόστος ανά μονάδα = (200,000 + 2*x)/x

Βρίσκουμε τα σημεία τομής των καμπυλών:

1η με 2η τεχνολογία:
$$\frac{20,000 + 20 \cdot x}{x} = \frac{10,000 + 40 \cdot x}{x} \iff 20\cdot x = 10,000 \iff x = 500$$

1η με 3η τεχνολογία:
$$\frac{20,000 + 20 \cdot x}{x} = \frac{100,000 + 4 \cdot x}{x} \iff 16 \cdot x = 80,000 \iff x = 5,000$$

3η με 4η τεχνολογία:
$$\frac{100,000 + 4 \cdot x}{x} = \frac{200,000 + 2 \cdot x}{x} \iff 2 \cdot x = 100,000 \iff x = 50,000$$

Επομένως προκύπτουν τα διαστήματα

| φθηνότερη τεχνολογία | από | μέχρι
| ------ | ------ |-----
| 2η | 0 | 500
| 1η | 501 | 5,000
| 3η | 5,001 | 50,000
| 4η | 50,001 | $$\infty$$


Για να εξαφανιστεί η 1η τεχνολογία πρέπει για $x=5,000$ να ισχύει:
$$ \frac{10,000 + (10+c)x}{x} = \frac{20,000 + 20 \cdot x}{x} \iff \frac{10,000 + (10+c)5,000}{5,000} = \frac{20,000 + 20 \cdot 5,000}{5,000}$$
$$ \iff 10,000 = 5,000 \cdot (10+c-20) \iff 2 = c-10 \iff c = 12$$

Άρα το κόστος πρέπει να πέσει από τα 30 στα 12 Ευρώ.

# 5η άσκηση

**(i)**
```verilog
module ask3_20_a(A,B,C,D,F);
	output F;
	input A,B,C,D;
	wire w1,w2,w3,w4,C_n;

	and G1(w1,C,D);
	and G2(w2,B,C_n);
	and G3(w4,w3,A);
	not G4(C_n,C);
	or G5(w3,w1,B);
	or G6(F,w2,w4);

endmodule

module ask3_21_b (A,B,C,D,F);
	input A,B,C,D;
	output F;
	wire w1,w2,w3,w4,w5,A_n,B_n,C_n;

	not
		G1(A_n,A),
		G2(B_n,B),
		G3(C_n,C),
		G4(F,w5);
	 
	nand
		G5(w1,A,B_n),
		G6(w2,A_n,B),
		G7(w3,C_n,D),
		G8(w4,w1,w2),
		G9(w5,w3,w4);
endmodule

module ask_3_24(A,B,C,D,E,F);
	input A,B,C,D,E;
	output F;
	wire w1,w2,E_not;

	not
		G1(E_not,E);

	nor
		G2(w1,A,B),
		G3(w2,C,D),
		G4(F,w1,w2,E_not);
endmodule

module ask_3_25 (A,B,C,D,F);
	input A,B,C,D;
	output F;
	wire w1,w2,w3,w4,A_not,B_not,D_not;

	not
		G1(A_not,A),
		G2(B_not,B),
		G3(D_not,D);

	nor
		G4(w1,A_not,B),
		G5(w2,A,B_not),
		G6(w3,C,D_not),
		G7(w4,w1,w2),
		G8(F,w3,w4);

endmodule
```

**(ii)**

```verilog
module ask3_20b(A,B,C,D,F);
	input A,B,C,D;
	output F;
	assign F = (((C ~& D) ~& (!B)) ~& A) ~& (B ~& (!C));
endmodule

module ask3_21a(A,B,C,D,F);
	input A,B,C,D;
	output F;
	assign F = ((A & !B) |  (!A & B)) & (C | !D);
endmodule

module ask3_24(A,B,C,D,E,F);
	input A,B,C,D,E;
	output F;
	assign F = !((A ~| B) | (C ~| D) | !E);
endmodule

module ask3_25(A,B,C,D,F);
	input A,B,C,D;
	output F;
	assign F = (((!A) ~| B) ~| (A ~| (!B))) ~| (C ~| (!D));
endmodule
```

# 6η Άσκηση

**(i) και (ii)**

```verilog
module ask4_36(D0,D1,D2,D3,x,y,V);
	input D0,D1,D2,D3;
	output x,y,V;
	
	wire w1,w2;
	
	not G1(w1,D2);
	and G2(w2,D1,w1);
	or  G3(y,D3,w2);
	
	or G4(x,D2,D3);
	or G5(V,x,D1,D0);
endmodule

module ask4_45(D0,D1,D2,D3,x,y,V);
	input D0,D1,D2,D3;
	output reg x,y,V;
	
	always@(D0,D1,D2,D3)
	begin
		if(D0 == 1) begin
			x = 0;y = 0;V = 1;
		end else if(D1 == 1) begin
			x = 0;y = 1;V = 1;
		end else if(D2 == 1) begin
			x = 1;y = 0;V = 1;
		end else if(D3 == 1) begin
			x = 1;y = 1;V = 1;
		end else begin
			x = 1'bx ;y = 1'bx;V = 0;
		end
	end
endmodule
```
