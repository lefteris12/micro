---
mainfont: Open Sans
documentclass: extarticle
fontsize: 12pt
geometry: margin=1in
papersize: a4
title: "1η-2η εργαστηριακή άσκηση (AVR) και 4η Ομάδα Ασκήσεων"
author:
- "Ελευθέριος Καπελώνης 03116163"
- "Κωνσταντίνος Αγιάννης 03116352"
---

# Ζήτημα 2.1
```asm
.include "m16def.inc"

reset:
ldi r16 , low(RAMEND)		; initialize stack pointer
out SPL , r16
ldi r16 , high(RAMEND)
out SPH , r16

ldi r24 , low(500)			; load r25:r24 with 500
ldi r25 , high(500)

ser r16						; initialize PORTB for output
out DDRB , r16
clr r16						; initialize PORTA for input
out DDRA , r16

ldi r17 , 1					; start from LSB

left:
out PORTB , r17				; turn on led

wait_1:
in r18 , PINA				; read input from push button PA0
bst r18 , 0					; transfer bit 0 of input PORTA to flag T
brts wait_1					; if flag T is set wait

ldi r24 , low(500)			; load r25:r24 with 500
ldi r25 , high(500)

rcall wait_msec				; delay for 500 msecs (0.5 sec)
lsl r17						; rotate left
cpi r17 , 0b10000000		; check if led is on MSB
breq right					; change direction
rjmp left

right:
out PORTB , r17				; turn on led

wait_2:
in r18 , PINA				; read input from push button PA0
bst r18 , 0					; transfer bit 0 of input PORTA to flag T
brts wait_2					; if flag T is set wait

ldi r24 , low(500)			; load r25:r24 with 500
ldi r25 , high(500)

rcall wait_msec				; delay for 500 msecs (0.5 sec)
lsr r17						; rotate right
cpi r17 , 0b000000001		; check if led is on LSB
breq left					; change direction
rjmp right

wait_msec:
push r24				
push r25				
ldi r24 , low(998)		
ldi r25 , high(998)		
rcall wait_usec			
pop r25					
pop r24					
sbiw r24 , 1			
brne wait_msec			
ret						

wait_usec:
sbiw r24 ,1				
nop						
nop						
nop						
nop						
brne wait_usec
ret					
```

# Ζήτημα 2.2 σε assembly

```asm
.include "m16def.inc"

reset: 
	ldi r24 , low(RAMEND) ; initialize stack pointer
	out SPL , r24
	ldi r24 , high(RAMEND)
	out SPH , r24
	ser r24 
	out DDRB , r24 ; PORTB is output
	clr r24
	out DDRA,r24 ; PORTA is input

main:
	
	in r24, PINA ; read PORTA
	mov r23,r24
	; andi r23,1 ; r23 = A

	lsr r24
	mov r22,r24
	
	; andi r22,1 ; r22 = B

	lsr r24
	mov r21,r24
	; andi r21,1 ; r21 = C
	
	lsr r24
	mov r20,r24
	; andi r20,1 ; r20 = D
	
	; compute F0
	mov r19,r23
	; and r19,r23
	and r19,r22
	and r19,r21 ; r19 = ABC
	
	mov r18, r21
	com r18
	; andi r18,1
	
	and r18,r20 ; r18 = C'D
	
	or r19,r18
	com r19 
	andi r19,1; r19 = F0
	
	; compute F1
	or r23,r22
	or r21,r20
	and r21,r23
	andi r21, 1 ; r21 = F1
	
	lsl r21
	or r21,r19
	out PORTB,r21

	rjmp main ; infinite loop
```

# Ζήτημα 2.2 σε C
```C
#include <avr/io.h>

unsigned char a, b, c, d;
unsigned char f0, f1;

int main(void)
{
    DDRB = 0xff;	// initialize PORTB as output
	DDRA = 0x00;	// initialize PORTA as input
	
    while (1) 
    {
		a = PINA;
		b = (PINA >> 1);
		c = (PINA >> 2);
		d = (PINA >> 3);
		
		f0 = ~(a&b&c | (~c)&d);	// F0 = (ABC+ C'D)'
		f0 = f0 & 0x01;
		f1 = (a|b) & (c|d);		// F1 = (A+B)(C+D)
		f1 = (f1 & 0x01) << 1;
		
		PORTB = f0 | f1;
    }
}
```

# Ζήτημα 2.3
```C
#include <avr/io.h>
unsigned char x;
int main(void) {
	DDRC=0x00; 
	DDRA=0xFF; 
	x = 1; 
	while(1) { 
		if ((PINC & 0x01) != 0){
			while ((PINC & 0x01) != 0); 
			x = (x << 1) | (x >> 7);
		} else if((PINC & 0x02) != 0){
			while ((PINC & 0x02) != 0); 
			x = (x >> 1) | (x << 7); 
		} else if((PINC & 0x04) != 0){
			while ((PINC & 0x04) != 0); 
			x = 128; //MSB
		}else if ((PINC & 0x08) != 0){
			while ((PINC & 0x08) != 0); 
			x = 1; //LSB
		}
		PORTA = x; 
	} 
	return 0;
}
```
