.include "m16def.inc"

reset:
ldi r16 , low(RAMEND)		; initialize stack pointer
out SPL , r16
ldi r16 , high(RAMEND)
out SPH , r16

ldi r24 , low(500)			; load r25:r24 with 500
ldi r25 , high(500)

ser r16						; initialize PORTB for output
out DDRB , r16
clr r16						; initialize PORTA for input
out DDRA , r16

ldi r17 , 1					; start from LSB

left:
out PORTB , r17				; turn on led

wait_1:
in r18 , PINA				; read input from push button PA0
bst r18 , 0					; transfer bit 0 of input PORTA to flag T
brts wait_1					; if flag T is set wait

ldi r24 , low(500)			; load r25:r24 with 500
ldi r25 , high(500)

rcall wait_msec				; delay for 500 msecs (0.5 sec)
lsl r17						; rotate left
cpi r17 , 0b10000000		; check if led is on MSB
breq right					; change direction
rjmp left

right:
out PORTB , r17				; turn on led

wait_2:
in r18 , PINA				; read input from push button PA0
bst r18 , 0					; transfer bit 0 of input PORTA to flag T
brts wait_2					; if flag T is set wait

ldi r24 , low(500)			; load r25:r24 with 500
ldi r25 , high(500)

rcall wait_msec				; delay for 500 msecs (0.5 sec)
lsr r17						; rotate right
cpi r17 , 0b000000001		; check if led is on LSB
breq left					; change direction
rjmp right

wait_msec:
push r24				
push r25				
ldi r24 , low(998)		
ldi r25 , high(998)		
rcall wait_usec			
pop r25					
pop r24					
sbiw r24 , 1			
brne wait_msec			
ret						

wait_usec:
sbiw r24 ,1				
nop						
nop						
nop						
nop						
brne wait_usec
ret					
