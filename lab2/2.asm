.include "m16def.inc"

reset: 
	ldi r24 , low(RAMEND) ; initialize stack pointer
	out SPL , r24
	ldi r24 , high(RAMEND)
	out SPH , r24
	ser r24 
	out DDRB , r24 ; PORTB is output
	clr r24
	out DDRA,r24 ; PORTA is input

main:
	
	in r24, PINA ; read PORTA
	mov r23,r24
	; andi r23,1 ; r23 = A

	lsr r24
	mov r22,r24
	
	; andi r22,1 ; r22 = B

	lsr r24
	mov r21,r24
	; andi r21,1 ; r21 = C
	
	lsr r24
	mov r20,r24
	; andi r20,1 ; r20 = D
	
	; compute F0
	mov r19,r23
	; and r19,r23
	and r19,r22
	and r19,r21 ; r19 = ABC
	
	mov r18, r21
	com r18
	; andi r18,1
	
	and r18,r20 ; r18 = C'D
	
	or r19,r18
	com r19 
	andi r19,1; r19 = F0
	
	; compute F1
	or r23,r22
	or r21,r20
	and r21,r23
	andi r21, 1 ; r21 = F1
	
	lsl r21
	or r21,r19
	out PORTB,r21

	rjmp main ; infinite loop
