#include <avr/io.h>
unsigned char x;
int main(void) {
	DDRC=0x00; // Αρχικοποίηση PORTC ως input
	DDRA=0xFF; // Αρχικοποίηση PORTA ως output
	x = 1; // Αρχικοποίηση μεταβλητής για αρχικά αναμμένο led
	while(1) { 
		if ((PINC & 0x01) != 0){ // Έλεγχος πατήματος push-button SW 
			while ((PINC & 0x01) != 0); // Έλεγχος επαναφοράς push-button S
			x = (x << 1) | (x >> 7); //Αριστερά κυκλικά
		} else if((PINC & 0x02) != 0){
			while ((PINC & 0x02) != 0); // Έλεγχος επαναφοράς push-button S
			x = (x >> 1) | (x << 7); //Δεξιά κυκλικά
		} else if((PINC & 0x04) != 0){
			while ((PINC & 0x04) != 0); // Έλεγχος επαναφοράς push-button S
			x = 128; //MSB
		}else if ((PINC & 0x08) != 0){
			while ((PINC & 0x08) != 0); // Έλεγχος επαναφοράς push-button S
			x = 1; //LSB
		}
		PORTA = x; // Έξοδος σε PORTA 
	} 
	return 0;
}
