#include <avr/io.h>

unsigned char a, b, c, d;
unsigned char f0, f1;

int main(void)
{
    DDRB = 0xff;	// initialize PORTB as output
	DDRA = 0x00;	// initialize PORTA as input
	
    while (1) 
    {
		a = PINA;
		b = (PINA >> 1);
		c = (PINA >> 2);
		d = (PINA >> 3);
		
		f0 = ~(a&b&c | (~c)&d);	// F0 = (ABC+ C'D)'
		f0 = f0 & 0x01;
		f1 = (a|b) & (c|d);		// F1 = (A+B)(C+D)
		f1 = (f1 & 0x01) << 1;
		
		PORTB = f0 | f1;
    }
}
