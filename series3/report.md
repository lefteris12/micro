---
mainfont: Open Sans
documentclass: extarticle
fontsize: 12pt
geometry: margin=1in
papersize: a4
title: "3η Σειρά ασκήσεων στα Συστήματα Μικροϋπολογιστών"
author:
- "Ελευθέριος Καπελώνης 03116163"
- "Κωνσταντίνος Αγιάννης 03116352"
---

# 1η ΑΣΚΗΣΗ
```asm
IN 10H          ; disable memory protection

MVI A,0DH       ; set interrupt's mask
SIM             ; apply mask
EI              ; enable interrupts

LOOP1:
JMP LOOP1       ; do nothing until an interrupt

INTR_ROUTINE:
POP H           ; H is useless
MVI A,00H
STA 3000H       ; turn on the leds
EI              ; enable interrupts
MVI B,3CH       ; B holds the seconds left 3CH = 60(10)

NEXT_SECOND:
LXI D,0A00H
LXI H,0A00H

MVI M,10H       ; 4 spaces
INX H
MVI M,10H
INX H
MVI M,10H
INX H
MVI M,10H
INX H

; isolate least significant digit
MVI A,0FH
ANA B           
MOV M,A
INX H

; isolate most significant digit
MVI A,F0H
ANA B
RAR
RAR
RAR
RAR
MOV M,A

PUSH B          ; save B to stack
CALL STDM       ; update display
MVI D,14H       ; 20 x 50 ms = 1 second
LXI B,0032H     ; used for delb

KEEP_DISPLAY:
CALL DCD
CALL DELB       ; wait 10 ms
DCR D
JNZ KEEP_DISPLAY

POP B           ; get B from stack
DCR B
JNZ NEXT_SECOND

MVI A,FFH       ; turn off the leds
STA 3000H
JMP LOOP1
END
```

# 2η ΑΣΚΗΣΗ
```asm
IN 10H          ; disable memory protection

MVI A,0DH       ; set interrupt's mask
SIM             ; apply mask
EI              ; enable interrupts

LOOP1:
JMP LOOP1       ; do nothing until an interrupt

INTR_ROUTINE:
POP H           ; H is useless

MVI B,0AH       ; example initial values for B and C
MVI C,60H

LXI H,0A00H

CALL KIND       ; least significant digit
MOV M,A
INX H
MOV D,A         ; save least significant digit to D

CALL KIND       ; most significant digit
MOV M,A
INX H
RAL             ; save the number to A
RAL
RAL
RAL
ORA D

MVI M,10H       ; 4 spaces
INX H
MVI M,10H
INX H
MVI M,10H
INX H
MVI M,10H

; compare A with B
CMP B
JZ FIRST_LED
JNC A_GREATER_B
FIRST_LED:
MVI A,FEH
STA 3000H
JMP LEDS_DONE

; compare A with C
A_GREATER_B:
CMP C
JZ SECOND_LED
JNC THIRD_LED
SECOND_LED:
MVI A,FDH
STA 3000H
JMP LEDS_DONE

THIRD_LED:
MVI A,FBH
STA 3000H
JMP LEDS_DONE

LEDS_DONE:
LXI D,0A00H
CALL STDM
CALL DCD
EI              ; enable interrupts
JMP LOOP1

END
```

# 3η ΑΣΚΗΣΗ

```asm
SWAP_NIBBLE MACRO REG
    PUSH PSW
    MOV A,M
    RAR
    RAR
    RAR
    RAR
    MOV M,A
    MOV A,REG
    RAR
    RAR
    RAR
    RAR
    MOV REG,A
    POP PSW
ENDM

FILL MACRO ADDR, LENGTH, K
    PUSH H ;store H,L
    PUSH B ;store B,C
    LXI H,ADDR ;POINT TO THE FIRST “ARRAY” ELEMENT
    MVI C,LENGTH ;STORE “ARRAY” LENGTH
    FILL_ARRAY:
    MVI M,K ;WRITE TO “ARRAY”
    INX H ;POINT TO THE NEXT “ARRAY” CELL
    DCR C
    JNZ FILL_ARRAY ;IF WHOLE “ARRAY” IS FILLED RETURN
    POP B ;get from stack
    POP H
ENDM

RHLL MACRO n
	MVI B,n
	LEFT_SHIFT:
	MOV A,L
	RAL
	MOV L,A
	MOV A,H
	RAL
	MOV H,A
	DCR B
	JNZ LEFT_SHIFT
ENDM
```

# 4η ΑΣΚΗΣΗ

Αφού ξεκίνησε να εκτελείται η εντολή, ολοκληρώνει, αποθηκεύει στο stack το PC και το νέο PC είναι το 3000h.
Άρα SP = 0x3ffe και PC = 0x3000.

Μετά γίνεται το interrupt 5.5, αποθηκεύται το 0x3000 (PC) στο stack, το PC γίνεται 2CH και o SP 0x3ffc.
Το Stack περιέχει στην κορυφή 0x00, 0x30, 0x00, 0x20.

Εκτελείται η ρουτίνα εξυπηρέτησης της διακοπής.
Στο τέλος της ρουτίνας γίνεται επαναφορά του PC (0x3000) από το stack.

Άρα εκτελείται η υπορουτίνα στη θέση μνήμης 0x3000.

# 5η ΑΣΚΗΣΗ

```asm
IN 10H          ; disable memory protection

MVI A,0EH       ; set interrupt's mask
SIM             ; apply mask
MVI B,00H		; if B==0 LSB is transferred, if B==1 MSB is transferred
MVI C,20H		; C stores how many number are left to add
MVI D,00H		; DE stores the current number
LXI H,0000H		; HL stores the sum of the numbers
EI              ; enable interrupts

LOOP1:
JMP LOOP1       ; do nothing until an interrupt

INTR_ROUTINE:
MOV A,B
ADI 00H
JNZ B_IS_ONE	; check if LSB or MSB

IN PORT_IN		; read from input port
ANI 0FH			; isolate 4 LSB
MOV E,A			; store LSB to E
MVI B,01H		; next 4 bits are MSB
EI				; enable interrupts
RET				; wait for an interrupt

B_IS_ONE:
IN PORT_IN
MVI B,00H
RLC
RLC
RLC
RLC
ANI F0H			; isolate 4 MSB
ORA E			
MOV E,A			; store MSB to E
DAD D			; add DE to HL
DCR C
JZ CALC_RES		; if all numbers are read calculate result
EI				; enable interrupts
RET				; wait for an interrupt

CALC_RES:
MOV A,H
RLC
RLC
RLC
ANI F8H			; isolate 5 MSB
MOV B,A
MOV A,L
RLC
RLC
RLC
ANI 07H			; isolate 3 LSB
ORA B			; A has the result
```
