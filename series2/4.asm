start: nop

;in 2h
LDA 2000h ; A = MEM[2000h]

MOV B,A

RLC
MOV C,A
RLC
ANA C
ANI 01h ; keep only last bit

RLC ; keep last bit for next operation
mov D,A ; D is A3&B3,0

;C <<= 2
MOV A,C
RLC
RLC
MOV C,A 

RLC
ANA C
ANI 01h ; keep only last bit,
;now lsb of A has A2 & B2

ORA D ; merge A and D
RLC ; keep last bit for next operation
mov D,A ; D = A3 & B3,A2&B2,0

;C <<= 2
MOV A,C
RLC
RLC
MOV C,A

RLC
ORA C
ANI 01h

ORA D ; merge A and D
RLC ; keep last bit for next operation
mov D,A ; D = A3 & B3,A2&B2,A1 | B1,0


;C <<= 2
MOV A,C
RLC
RLC
MOV C,A 

RLC
ORA C
ANI 01h ; keep only last bit

ORA D ; merge A and D

mov D,A ; D = A3&B3,A2&B2,A1|B1,A0|B0

RRC
ANI 01h
XRA D ; A = A3&B3,A2&B2,A1|B1,(A0|B0)^(A1|B1)

;out 3h
CMA
STA 3000h ; MEM[3000h] = A
jmp start
END
