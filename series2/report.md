---
mainfont: Open Sans 
documentclass: extarticle
fontsize: 12pt
geometry: margin=1in
papersize: a4
title: "2η Σειρά ασκήσεων στα Συστήματα Μικροϋπολογιστών"
author:
- "Ελευθέριος Καπελώνης 03116163"
- "Κωνσταντίνος Αγιάννης 03116352"
---
# 1η ΑΣΚΗΣΗ

```asm
PART_A:
IN 10H          ; disable memory protection
LXI H,0900H     ; load starting address
MVI A,00H       ; A holds the ascending numbers

LD_NUMS_TO_MEM:
MOV M,A         ; store number to memory
INR A
INX H
CPI 00H         ; if A == 0 all numbers have been stored
JZ PART_B     
JMP LD_NUMS_TO_MEM

PART_B:
LXI H,0900H     ; load starting address
LXI B,0000H     ; B holds the count of binary ones
MVI E,00H       ; E holds the bit we must check 
COUNT_ONES:
MOV A,E
CPI 08H         ; if E==08H check next number
JZ NEXT_NUMBER
MOV A,M         ; read number from memory
INR E
RLC             ; move next bit to CY
JNC COUNT_ONES  
INX B           ; if bit is 1 increase count   
JMP COUNT_ONES
NEXT_NUMBER:
INX H
MOV A,H
CPI 0AH         ; check if all numbers have been read
JZ PART_C
MVI E,00H       ; reset E to 0
JMP COUNT_ONES

PART_C:
LXI H,0900H     ; load starting address
MVI D,00H       ; D holds the count of the numbers between 10H and 60H
IN_RANGE:
MOV A,H
CPI 0AH         ; check if all numbers have been read
JZ _END
MOV A,M         ; load number
INX H
CPI 10H
JC IN_RANGE     ; if number < 10H continue 
CPI 60H         
JNZ NOT_EQUAL   
INR D           ; if number == 60H increase D
JMP IN_RANGE
NOT_EQUAL:
JNC IN_RANGE
INR D
JMP IN_RANGE    ; if number < 60H increase D
_END:
END
```

# 2η ΑΣΚΗΣΗ

```asm
IN 10H      ; disable memory protection
LXI B,00C8H ; (BC)=C8H=200(10)
MVI D,96H   ; use D to count 150(10)=96H calls to DELB, so 30 sec

STATE1:
LDA 2000H   ; load input from dip switches
RLC         ; copy MSB to CY
JNC STATE2  ; dip switch OFF
CALL DELB
DCR D
JNZ STATE1 
MVI A,FFH   ; if 30 secs have passed turn off the leds
STA 3000H
JMP STATE1

STATE2:     ; OFF
LDA 2000H   ; load input from dip switches
RLC         ; copy MSB to CY
JC STATE3   ; dip switch OFF-ON
CALL DELB
DCR D
JNZ STATE2 
MVI A,FFH   ; if 30 secs have passed turn off the leds
STA 3000H
JMP STATE2

STATE3:     ; OFF-ON
LDA 2000H   ; load input from dip switches
RLC         ; copy MSB to CY
JNC COMPLETED ; dip switch OFF-ON-OFF
CALL DELB
DCR D
JNZ STATE3 
MVI A,FFH   ; if 30 secs have passed turn off the leds
STA 3000H
JMP STATE3

COMPLETED:  ; OFF-ON-OFF
MVI A,00H   ; turn on the leds
STA 3000H
MVI D,96H   ; reset D
JMP STATE1
END
```

# 3η ΑΣΚΗΣΗ

## (i)

```asm
IN 10H      ; disable memory protection
MAIN_LOOP:
LDA 2000H   ; get input from dip switches
MVI B,09H   ; use B to find the first bit from the right with value 1 
LOOP1:
DCR B
JNZ CONTINUE
MVI A,FFH   ; if B is 0 that means that all dip switches are off so turn off leds
STA 3000H
JMP MAIN_LOOP
CONTINUE:
RRC         ; check if next bit is 1 
JNC LOOP1
END_LOOP1:

MVI A,FFH   ; all leds are off
LOOP2:
STC         
CMC         ; set CY to 0
RAR         ; open next led
DCR B
JZ END_LOOP2
JMP LOOP2
END_LOOP2:
STA 3000H
JMP MAIN_LOOP   ; infinite execution of the program
END
```

## (ii)

```asm
IN 10H      ; disable memory protection

START:
CALL KIND   ; get keyboard input and save it to A
ADI 00H     ; updates flag
JZ START    ; check if input is 0
CPI 09H 
JNC START   ; check if input is greater than or equal to 9

MOV B,A
MVI A,01H   ; used to show which led to open    
LOOP1:
DCR B
JZ END_LOOP1
RLC
JMP LOOP1
END_LOOP1:
CMA
STA 3000H   ; open the led
JMP START   ; infinite execution of the program

END
```

## (iii)

```asm
IN 10H      ; disable memory protection
JMP NEW_BUTTON
START:
MVI B,00H   ; B and C are used to store the button code
MVI C,00H
MVI A,FBH   ; scan port
STA 2800H   ; store to scan port 
LDA 1800H   ; load result
ANI 07H     ; MSB are set to 0
CPI 06H     ; button 0
JZ NEW_BUTTON   ; if a button is pressed output result
INR B

MVI A,F7H
STA 2800H
LDA 1800H
ANI 07H
CPI 06H     ; button 1
JZ NEW_BUTTON
INR B
CPI 05H     ; button 2
JZ NEW_BUTTON
INR B
CPI 03H     ; button 3
JZ NEW_BUTTON
INR B

MVI A,EFH
STA 2800H
LDA 1800H
ANI 07H
CPI 06H     ; button 4
JZ NEW_BUTTON
INR B
CPI 05H     ; button 5
JZ NEW_BUTTON
INR B
CPI 03H     ; button 6
JZ NEW_BUTTON
INR B

MVI A,DFH
STA 2800H
LDA 1800H
ANI 07H
CPI 06H     ; button 7
JZ NEW_BUTTON
INR B
CPI 05H     ; button 8
JZ NEW_BUTTON
INR B
CPI 03H     ; button 9
JZ NEW_BUTTON
INR B

MVI A,BFH
STA 2800H
LDA 1800H
ANI 07H
CPI 06H     ; button A
JZ NEW_BUTTON
INR B
CPI 05H     ; button B
JZ NEW_BUTTON
INR B
CPI 03H     ; button C
JZ NEW_BUTTON
INR B

MVI A,7FH
STA 2800H
LDA 1800H
ANI 07H
CPI 06H     ; button D
JZ NEW_BUTTON
INR B
CPI 05H     ; button E
JZ NEW_BUTTON
INR B
CPI 03H     ; button F
JZ NEW_BUTTON

MVI C,08H

MVI A,FEH
STA 2800H
LDA 1800H
ANI 03H
MVI B,06H
CPI 02H     ; button INSTR_STEP
JZ NEW_BUTTON
MVI B,05H
CPI 01H     ; button FETCH PC
JZ NEW_BUTTON


MVI A,FDH
STA 2800H
LDA 1800H
ANI 07H
MVI B,04H
CPI 06H     ; button RUN
JZ NEW_BUTTON
MVI B,00H
CPI 05H     ; button FETCH REG
JZ NEW_BUTTON
MVI B,02H
CPI 03H     ; button FETCH ADDR
JZ NEW_BUTTON

MVI A,FBH
STA 2800H
LDA 1800H
ANI 07H
MVI B,03H
CPI 05H     ; button STORE/INCR
JZ NEW_BUTTON
MVI B,01H
CPI 03H     ; button DECR
JZ NEW_BUTTON

JMP UPDATE_DISPLAY

NEW_BUTTON:
LXI H,0A00H
LXI D,0A00H

; 4 spaces
MVI M,10H
INX H
MVI M,10H
INX H
MVI M,10H
INX H
MVI M,10H
INX H

; the LSB digit in register B
MOV M,B
INX H

; the MSB digit in register C
MOV M,C

CALL STDM

UPDATE_DISPLAY:
CALL DCD
JMP START

END
```

# 4η ΑΣΚΗΣΗ

```asm
start: nop

;in 2h
LDA 2000h ; A = MEM[2000h]

MOV B,A

RLC
MOV C,A
RLC
ANA C
ANI 01h ; keep only last bit

RLC ; keep last bit for next operation
mov D,A ; D is A3&B3,0

;C <<= 2
MOV A,C
RLC
RLC
MOV C,A 

RLC
ANA C
ANI 01h ; keep only last bit,
;now lsb of A has A2 & B2

ORA D ; merge A and D
RLC ; keep last bit for next operation
mov D,A ; D = A3 & B3,A2&B2,0

;C <<= 2
MOV A,C
RLC
RLC
MOV C,A

RLC
ORA C
ANI 01h

ORA D ; merge A and D
RLC ; keep last bit for next operation
mov D,A ; D = A3 & B3,A2&B2,A1 | B1,0


;C <<= 2
MOV A,C
RLC
RLC
MOV C,A 

RLC
ORA C
ANI 01h ; keep only last bit

ORA D ; merge A and D

mov D,A ; D = A3&B3,A2&B2,A1|B1,A0|B0

RRC
ANI 01h
XRA D ; A = A3&B3,A2&B2,A1|B1,(A0|B0)^(A1|B1)

;out 3h
CMA
STA 3000h ; MEM[3000h] = A
jmp start
END
```

# 5η ΑΣΚΗΣΗ

Για τη διευθυνσιοδότηση 256 θέσεων μνήμης απαιτούνται 8 bits ($A_0 - Α_7$). Τα bits ($Α_4 - Α_7$) χρησιμοποιούνται για την επιλογή μια από τις 16 γραμμές και τα bits ($Α_0 - Α_3$) για την επιλογή μιας από τις 16 τετράδες κάθε γραμμής.

## Παράδειγμα ανάγνωσης
Ας υποθέσουμε ότι θέλουμε να κάνουμε ανάγνωση τη θέση μνήμης με διεύθυνση $10010101$. Τα σήματα $\overline{CS}$ και $\overline{RD}$ ενεργοποιούνται άρα γίνονται 0 και το σήμα $\overline{WE}$ απενεργοποιείται άρα γίνεται 1. Με αυτό τον τρόπο πραγματοποιείται ανάγνωση. 

Τα MSB $1001$ ($Α_4 - Α_7$) καθορίζουν τη γραμμή επιλογής η οποία είναι η 9. Τα LSB $0101$ ενεργοποιούν την 5η στήλη από κάθε μια από τις 4 16-άδες bits μέσω των πολυπλεκτών. Έτσι τα Ι/Ο1 - I/O4 λαμβάνουν τα 4 αυτά bits.

## Παράδειγμα εγγραφής
Εντελώς αντίστοιχα λειτουργεί και η εγγραφή. Ενεργοποιούνται (γίνονται 0) τα σήματα $\overline{CS}$ και $\overline{WE}$ και απενεργοποιείται (γίνεται 1) το σήμα $\overline{RD}$. Έτσι τώρα τα 4 bit Ι/Ο1 - I/O4 οδηγούνται μέσω του ελέγχου δεδομένων εισόδου και των πολυπλεκτών στη μνήμη και έτσι γίνεται εγγραφή των δεδομένων.

![Οργάνωση SRAM](ask5.jpg)

# 6η ΑΣΚΗΣΗ

Ο χάρτης μνήμης είναι ο εξής:

| hex address | binary address A15-A0 | decoder address Α13-Α11|
| ------ | ------ | ---- |
**0x0000**  |  0000000000000000  |  000
**0x0fff**  |  0000111111111111  |  001
**0x1000**  |  0001000000000000  |  010
**0x1fff**  |  0001111111111111  |  011
**0x2000**  |  0010000000000000  |  100
**0x27ff**  |  0010011111111111  |  100
**0x2800**  |  0010100000000000  |  101
**0x37ff**  |  0011011111111111  |  110
**0x3800**  |  0011100000000000  |  111
**0x3fff**  |  0011111111111111  |  111

![Σχεδιάγραμμα του μικροϋπολογιστικού συστήματος με τα σήματα](ask6.jpg)

# 7η ΑΣΚΗΣΗ

| hex address | binary address A15-A0 | decoder address Α14-Α12|
| ------ | ------ | ---- |
**0x0000**  |  0000000000000000  |  000
**0x1fff**  |  0001111111111111  |  001
**0x2000**  |  0010000000000000  |  010
**0x2fff**  |  0010111111111111  |  010
**0x3000**  |  0011000000000000  |  011
**0x3fff**  |  0011111111111111  |  011
**0x4000**  |  0100000000000000  |  100
**0x4fff**  |  0100111111111111  |  100
**0x5000**  |  0101000000000000  |  101
**0x6fff**  |  0110111111111111  |  110

Παρατηρούμε ότι τα bit 12-14 μπορούν να καθορίσουν το τσιπάκι στο οποίο θα προσπελάσονται τα δεδομένα.

![Σχεδιάγραμμα του μικροϋπολογιστικού συστήματος](ask7.jpg)


